/*
 * Chip-specific header file for the AT91SAM9G25 family
 *
 *  Copyright (C) 2015 Tom Hollingworth
 *
 * Common definitions.
 * Based on AT91SAM9G25 datasheet.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
 
 #ifndef AT91SAM9G25_H
 #define AT91SAM9G25_H
 
 /*
  * User Peripheral physical base addresses.
  */
 #define AT91SAM9G25_BASE_TC0		0xf8008000 // TCB0 Channel 0
 #define AT91SAM9G25_BASE_TC1		0xf800c000 // TCB0 Channel 1
 #define AT91SAM9G25_BASE_TC2		0xf8008080 // TCB0 Channel 2
 #define AT91SAM9G25_BASE_TC3		0xf800c000 // TCB1 Channel 0
 #define AT91SAM9G25_BASE_TC4		0xf800c040 // TCB1 Channel 1
 #define AT91SAM9G25_BASE_TC5		0xf800c040 // TCB1 Channel 2
 
 #endif