/*
 * Kernel Frequency Counter on Timer/Counter Block 1, Channel 0 (TCLK3)
 * Author  : Tom Hollingworth
 * Date    : 2015.04.04
 * Version : 1.00
 * License : GPL
 */


#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/io.h>
#include <linux/interrupt.h>
#include <linux/clk.h>
#include <linux/atmel_tc.h>
#include <linux/timer.h>
#include <linux/proc_fs.h>

#include <asm/uaccess.h>

void __iomem *tcaddr; 
static int s_Period = 1000;
static long counter_value = 0;
static struct timer_list s_PeriodicTimer;
static struct proc_dir_entry* fcounter_file;
int len,temp;
char *msg;
char freq[10];

static void PeriodicTimerHandler(unsigned long unused){
	counter_value = __raw_readl(tcaddr+ATMEL_TC_CV);			//Read Counter Value
	// printk(KERN_INFO "fcounter: value=%lu\n",counter_value);
	sprintf(freq, "%lu/n", counter_value);
	msg = freq;
	len=strlen(msg);
	temp=len;
	__raw_writel(ATMEL_TC_SWTRG, tcaddr + ATMEL_TC_CCR);			// Software trigger reset
	mod_timer(&s_PeriodicTimer, jiffies+msecs_to_jiffies(s_Period));	// Restart Timer
}

static int fcounter_read(struct file *filp,char *buf,size_t count,loff_t *offp ){
	int result;
	if(count>temp){
		count=temp;
	}
	temp=temp-count;
	result = copy_to_user(buf,msg,count);
	if(count==0)
		temp=len; 
	return count;
}

static const struct file_operations fcounter_fops = {
	.owner		= THIS_MODULE,
	.read		= fcounter_read,
};

static int __init fcounter_init(void)
{
	int result;
	struct atmel_tc *tc;
	struct clk *t3_clk;

	printk(KERN_INFO "fcounter: intializing...\n");

	tc = atmel_tc_alloc(1);
	if (!tc) {
		return -ENODEV;
	}

	t3_clk = tc->clk[0];
	clk_enable(t3_clk);

	tcaddr = tc->regs;

	__raw_writel(ATMEL_TC_CLKDIS, tcaddr + ATMEL_TC_CCR);			// Disable Clock
	__raw_writel(0x0000000F, tcaddr + ATMEL_TC_IDR);			// Disable interrupts
	__raw_writel(ATMEL_TC_XC0, tcaddr + ATMEL_TC_REG(0, CMR));		// Set External Clocksource 0

	__raw_writel(0, tcaddr + ATMEL_TC_CV);					// Set clock to 0
	__raw_writel(ATMEL_TC_CLKEN, tcaddr + ATMEL_TC_CCR);			// Enable Clock
	__raw_writel(ATMEL_TC_SWTRG, tcaddr + ATMEL_TC_CCR);			// Software trigger reset

	
	setup_timer(&s_PeriodicTimer, PeriodicTimerHandler, 0);
	result = mod_timer(&s_PeriodicTimer, jiffies + msecs_to_jiffies(s_Period));
	if (result < 0){
		printk(KERN_WARNING "fcoutner: failed to start a timer\n");
		return -1;
	}

	fcounter_file = proc_create("fcounter",0,NULL,&fcounter_fops);
	if (!fcounter_file){
		return -ENOMEM;
	}

	printk(KERN_INFO "fcounter: initialized!\n");

	return 0;
}

static void __exit fcounter_exit(void)
{
	del_timer(&s_PeriodicTimer);
	remove_proc_entry("fcounter", NULL);
	printk(KERN_INFO "fcounter: module removed.\n");

}



module_init(fcounter_init);
module_exit(fcounter_exit);

MODULE_AUTHOR("Tom Hollingworth");
MODULE_DESCRIPTION("Hardware Frequency counter");
MODULE_LICENSE("GPL");
