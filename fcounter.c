/*
 * Kernel Frequency Counter on Timer/Counter Block 1, Channel 0 (TCLK3)
 * Author  : Tom Hollingworth
 * Date    : 2015.03.27
 * Version : 1.00
 * License : GPL
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/interrupt.h>
#include <linux/types.h>
#include <linux/init.h>
#include <linux/mm.h>
#include <linux/platform_device.h>
#include <linux/ioctl.h>
#include <asm/setup.h>
#include <asm/mach-types.h>

#include <asm/mach/arch.h>
#include <asm/mach/map.h>

#include <asm/uaccess.h>
#include <asm/mach/time.h>

#define AT91SAM9G25_TCB0	0xf8008000
#define AT91SAM9G25_TCB1	0xf800c000
#define AT91SAM9G25_TCB0_TC_BCR 0xf80080c0	// Block Control Register
#define AT91SAM9G25_TCB1_TC_BCR 0xf800c0c0	// Block Control Register
#define AT91SAM9G25_TCB0_TC_BMR 0xf80080c4	// Block Control Register
#define AT91SAM9G25_TCB1_TC_BMR 0xf800c0c4	// Block Control Register

#define AT91_TC0 		0x00
#define AT91_TC1 		0x40
#define AT91_TC2 		0x80

#define AT91_TC_CCR		0x00		// Channel Control Register
#define AT91_TC_CMR		0x04		// Channel Mode Register
#define AT91_TC_CV		0x10		// Counter Value
#define AT91_TC_RA 		0x14		// Register A
#define AT91_TC_RB 		0x18		// Regsiter B
#define AT91_TC_RC 		0x1c		// Register C
#define AT91_TC_SR 		0x20		// Status Register
#define AT91_TC_IER		0x24		// Interrupt Enable Register
#define AT91_TC_IDR		0x28		// Interrupt Disable Register
#define AT91_TC_IMR		0x2c		// Interrupt Mask Register

//static void __exit ad_irq_cleanup(void);

static volatile unsigned long ad_irq_count = 0;

#ifdef MODULE_AUTHOR
 MODULE_AUTHOR("Tom Hollingworth");
 MODULE_DESCRIPTION("Hardware Frequency counter");
 MODULE_LICENSE("GPL");
#endif

static struct timer_list s_PeriodicTimer;
static int s_Period = 1000;
static int counter_value = 0;

/* unload kernel module */
static void __exit counter_cleanup(void) {
	del_timer(&s_PeriodicTimer);
    printk(KERN_INFO "fcounter: module removed.\n");
}

/*
 * Read from Timerblock 1 registers.
 */
static inline unsigned long at91_tcb1_read(unsigned int reg){
	void __iomem *tcb1_base = (void __iomem *)AT91SAM9G25_TCB1;
	printk(KERN_INFO "fcounter: read p: %p + (%d)\n", tcb1_base, reg);

	return __raw_readl(tcb1_base + reg);
}

/*
 * Write to Timerblock 1 registers.
 */
static inline void at91_tcb1_write(unsigned int reg, unsigned long value){
	void __iomem *tcb1_base = (void __iomem *)AT91SAM9G25_TCB1;
		printk(KERN_INFO "fcounter: write p: %p + (%d)\n", tcb1_base, reg);

	__raw_writel(value, tcb1_base + reg);
}

/*
 * Read from Timerblock 0 registers.
 */
static inline unsigned long at91_tcb0_read(unsigned int reg)
{
	void __iomem *tcb2_base = (void __iomem *)AT91SAM9G25_TCB0;
	return __raw_readl(tcb2_base + reg);
}

/*
 * Write to Timerblock 0 registers.
 */
static inline void at91_tcb0_write(unsigned int reg, unsigned long value)
{
	void __iomem *tcb2_base = (void __iomem *)AT91SAM9G25_TCB0;
	__raw_writel(value, tcb2_base + reg);
}

static void PeriodicTimerHandler(unsigned long unused){
	//Read Counter Value
	counter_value = at91_tcb1_read(AT91_TC0 + AT91_TC_CV);
	printk(KERN_INFO "fcounter: value=%d",counter_value);
	//Restart Counter 
	at91_tcb1_write(AT91_TC0 + AT91_TC_CCR , 0x05 );
	// Restart Timer
	mod_timer(&s_PeriodicTimer, jiffies+msecs_to_jiffies(s_Period));
}

static int __init counter_init(void) {

	int result;
	printk(KERN_INFO "fcounter: setup registers\n");

	// disable clock: 
	at91_tcb1_write(AT91_TC0 + AT91_TC_CCR,  ((at91_tcb1_read(AT91_TC0 + AT91_TC_CCR) | 2) ) );

	// disable all Timer Channel 0 interrupts: 
	at91_tcb1_write(AT91_TC0 + AT91_TC_IDR, 0xFFFFFFFF );

	// read & clear status: 
	at91_tcb1_read(AT91_TC0 + AT91_TC_SR );

	// enable timer clock 5, reset counter and start clock
	at91_tcb1_write(AT91_TC0 + AT91_TC_CMR, 0x00000105 );

	printk(KERN_INFO "fcounter: CMR: %lu\n", at91_tcb1_read(AT91_TC0 + AT91_TC_CMR) );
	
	setup_timer(&s_PeriodicTimer, PeriodicTimerHandler, 0);
	result = mod_timer(&s_PeriodicTimer, jiffies + msecs_to_jiffies(s_Period));
	if (result < 0){
		printk(KERN_WARNING "fcounter: failed to start a timer");
		return -1;
	}

	// enable & start clock 
	at91_tcb1_write(AT91_TC0 + AT91_TC_CCR , 0x01 );
	at91_tcb1_write(AT91_TC0 + AT91_TC_CCR , 0x05 );

	printk(KERN_INFO "fcounter: clock started");
	return 0;
}

module_init(counter_init);
module_exit(counter_cleanup);
